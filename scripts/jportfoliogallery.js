//http://stackoverflow.com/questions/5414304/scrolling-a-div-of-images-horizontally-with-controlls
  $(document).ready(function(){
    setWidth();
    scrollDiv();
    });
    
  function scrollDiv(dir, px) {
    var scroller = document.getElementById("container");

    if (dir == "l") {
        scroller.scrollLeft -= px;
    }
    else if (dir == "r") {
        scroller.scrollLeft += px;
    }
  }
    
    function setWidth(){
    //this gets the number of children divs
      var kidNum = $(".galleryItem").length;
      var itemsDec = kidNum / 5;
      var galleryPercent = (itemsDec * 100).toFixed(2) + "%";
    
      $("#gallery").css("width", galleryPercent);
   
      var itemWidth = (16 * (1/itemsDec)).toFixed(2) + "%";
      $(".galleryItem").css("width", itemWidth);
    }  